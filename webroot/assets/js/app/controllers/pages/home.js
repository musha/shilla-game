// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(["vendor/zepto/zepto.html5Loader.min"],
                function() {
                    done();
                });
            },
            afterRender: function() {
                // 打开页面先不能点击游戏开始

                // 当前页面
                app.currentUrl = window.location.href;
                
                // 判断是正式还是测试，默认正式
                app.httpUrl = 'http://shilladfs.shjiaoai.com';
                if(window.location.host.indexOf('test') >= 0) {
                    // 测试
                    alert('测试');
                    app.httpUrl = 'http://shilladfs-test.shjiaoai.com';
                };

                app.openid = '', app.nickname = '', app.headimgurl = '';

                // 授权，获得微信信息(打开页面时调用)
                function isAu() {
                    $.ajax({
                        type: 'post',
                        url: app.httpUrl + '/interface/api.aspx?type=isau&r=' + Math.random(),
                        dataType: 'json',
                        data: {},
                        async: false,
                        cache: false,
                        success: function (result) {
                            console.log(result);
                            if (result.errcode == 0) {
                                // 未授权
                                window.location.href = '/interface/au.aspx?aubackurl=' + encodeURIComponent(app.currentUrl);
                            } else {
                                // 已授权
                                app.openid = result.openid;
                                app.nickname = result.nickname;//昵称
                                app.headimgurl = result.headimgurl;//头像
                                $.cookie('openid', app.openid, 365);
                            }
                        },
                        complete: function() {
                            // 授权完了再调用 打开首页时间和是否中奖
                            homePageTime(); // 记录每次打开首页的时间(打开首页时调用，放在授权接口之后)
                            isGift(); // 是否中奖
                        }
                    });
                };
                isAu(); 

                // 记录每次打开首页的时间(打开首页时调用，放在授权之后)
                function homePageTime() {
                    $.ajax({
                        type: 'post',
                        url: app.httpUrl + '/interface/api.aspx?type=homepagetime&r=' + Math.random(),
                        dataType: 'json',
                        data: { openid: $.cookie('openid') },
                        async: false,
                        cache: false,
                        success: function (result) {
                            console.log(result);
                            if (result.errcode == -1) {
                               // alert('未授权');
                            }
                        }
                    });
                };
                
                // 是否已经中奖(打开首页时调用，放在授权之后)
                function isGift() {
                    $.ajax({
                        type: 'post',
                        url: app.httpUrl + '/interface/api.aspx?type=isgift&r=' + Math.random(),
                        dataType: 'json',
                        data: { openid: $.cookie('openid') },
                        async: false,
                        cache: false,
                        success: function (result) {
                            console.log(result);
                            // 获取剩余抽奖次数
                            app.leftTime = result.leftnum;
                            if (result.errcode == -1) {
                                // 未授权
                                // 开始游戏
                                $('.start').on('click', function() {
                                    // gtag('event', 'play', {'event_category': 'airport', 'event_label': 'click'});
                                    alert('未授权');
                                });
                            } else if (result.errcode == 1) {
                                // 已中奖
                                app.giftid = result.giftid;
                                app.username = result.enname;
                                app.passport = result.passport;
                                app.mobile = result.phone;
                                app.gidx = result.gidx;
                                app.gguid = result.gguid;

                                // 已经填过信息
                                if(result.isinfo == 1) {
                                    $('.btn-apply').hide();
                                };
                                $('.modal-prize' + result.giftid).fadeIn('fast');
                            } else {
                                // 没有中过奖
                                if(result.leftnum == 3) {
                                    // 开始游戏
                                    $('.start').on('click', function() {
                                        gtag('event', 'play', {'event_category': 'airport', 'event_label': 'click'});
                                        tl.kill();
                                        app.router.goto('turntable');
                                    });
                                } else {
                                    // 5等奖
                                    $('.modal-prize5').fadeIn('fast');
                                    if(result.leftnum > 0) {
                                        // 再抽一次
                                       $('.btn-again').on('click', function() {
                                        gtag('event', 'retry', {'event_category': 'airport', 'event_label': 'click'});
                                            $('.modal').fadeOut('fast');
                                        });
                                        // 可以继续抽奖
                                        $('.start').on('click', function() {
                                            gtag('event', 'play', {'event_category': 'airport', 'event_label': 'click'});
                                            tl.kill();
                                            app.router.goto('turntable');
                                        });
                                    } else {
                                        // 不能继续抽奖
                                        $('.btn-again').on('click', function() {
                                            gtag('event', 'retry', {'event_category': 'airport', 'event_label': 'click'});
                                            tl.kill();
                                            app.router.goto('message-over');
                                        });
                                    }
                                }; 
                            }
                        }
                    });
                };

                function getParameterByName(name) {
                    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                        results = regex.exec(location.search);
                    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                };

                // 预加载
                var firstLoadFiles = {
                    "files": [
                        {
                            "type": "AUDIO",
                            "sources": {
                                "mp3": {
                                    "source": "assets/audio/music.mp3",
                                    "size": 1
                                }
                            },
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/bg0.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/bg-modal.png",
                            "size": 1
                        }, 
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/prize-pig.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/prize-samsung.png",
                            "size": 1
                        }, 
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/prize-stay.png",
                            "size": 1
                        }, 
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/prize-shopping.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/prize-coupon.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/coupons.png",
                            "size": 1
                        }, 
                        {
                            "type": "IMAGE",
                            "source": "assets/images/common/card.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/turntable/turnplate.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/turntable/pan.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/turntable/turnplate-button.png",
                            "size": 1
                        },
                        {
                            "type": "IMAGE",
                            "source": "assets/images/turntable/turnplate-pointer.png",
                            "size": 1
                        }
                    ]
                };
                $.html5Loader({
                    filesToLoad: firstLoadFiles,
                    onBeforeLoad: function() {},
                    onComplete: function() {

                    },
                    onElementLoaded: function(obj, elm) {},
                    onUpdate: function(percentage) {
                        // console.log(percentage);
                    }
                });

                // 自适应
                $('.modal').height($(window).height());
                // 调整手机屏幕尺寸 
                if ($(window).height() > 700) {
                    $('.title').css('top', '6%');
                    $('.prize').css('top', '30%');
                    $('.bottom').css('top', '86%');
                } else {
                    // 
                };

                var context = this;

                // 动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.home_page'), 0.5, { autoAlpha: 0, onComplete: function() {
                    // console.log('page home');
                } }, 0.1);

                // 领奖
               $('.btn-apply').on('click', function() {
                    tl.kill();
                    app.router.goto('policy', ['home']);
                });

               // 解锁福利
               $('.btn-unlock').on('click', function() {
                    tl.kill();
                    app.router.goto('prize-coupons', ['home']);
                });

                // 门店代金券
                $('.shop-coupon').on('click', function() {
                    gtag('event', 'coupon1', {'event_category': 'airport', 'event_label': 'click'});
                    app.router.goto('coupons', ['home']);
                });
                // 购物卡
                $('.shopping-card').on('click', function() {
                    gtag('event', 'coupon2', {'event_category': 'airport', 'event_label': 'click'});
                    app.router.goto('card', ['home']);
                });

                // 日期
                function FormatDate(strTime) {
                    var date = new Date(strTime);
                    return date.getFullYear() + "-" + (date.getMonth() + 1 ) + "-" + date.getDate();
                };

                // 报销出租车截止日期
                $('.taxi').on('click', function(e) {
                    gtag('event', 'coupon3', {'event_category': 'airport', 'event_label': 'click'});
                    var today = new Date();
                    var today_time =  FormatDate(today);
                    if(today_time > '2019-01-01'){
                        e.preventDefault();
                        tl.kill();
                        app.router.goto('message-fin', ['home']);
                    }
                });

                // 开通银卡
                $('.silver-card').on('click', function() {
                    gtag('event', 'coupon4', {'event_category': 'airport', 'event_label': 'click'});
                });

                // 分享
                $('.btn-share').on('click', function() {
                    gtag('event', 'share2', {'event_category': 'airport','event_label': 'click'});
                    tl.kill();
                    app.router.goto('message-share', ['home']);
                });

                // 标题滑入
                function titleSlideIn() {
                    var tl = new TimelineMax({ });
                    tl.to(context.$('.title1'), 0.8, { 'left': 0 });
                    tl.to(context.$('.title2'), 0.8, { 'left': 0 }, '-=0.8');
                };

                // 云朵飘动
                function cloudsDrift() {
                    var tl = new TimelineMax({ });
                    tl.to(context.$('.cloud1'), 4, { 'left': '0%', yoyo: true, repeat: -1, ease: Power0.easeNone }, 0);
                    tl.to(context.$('.cloud2'), 2, { 'left': '14.4%', yoyo: true, repeat: -1, ease: Power0.easeNone }, 0.5);
                    tl.to(context.$('.cloud3'), 4, { 'left': '46.5%', yoyo: true, repeat: -1, ease: Power0.easeNone }, 1);
                    tl.to(context.$('.cloud4'), 3, { 'left': '80.8%', yoyo: true, repeat: -1, ease: Power0.easeNone }, 0.5);
                };
            }
        })
        //  Return the module for AMD compliance.
        return Page;
    })