// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(['vendor/zepto/zepto.html5Loader.min'],
                function() {
                    done();
                });
            },
            afterRender: function() {

                // 自适应
                $('.modal').height($(window).height());
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                // 调整手机屏幕尺寸 
                if ($(window).height() < 600) {
                    //
                } else {
                    // 
                };

                var context = this;

                //动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.message-fin_page'), 0.5, { autoAlpha: 0, onComplete: function() {
                    // console.log('page message fin');
                } }, 0.1);

                // 返回首页
                context.$('.close').on('click', function() {
                    tl.kill();
                    app.router.goto(context.args[0]);
                });
            },
            resize: function(ww, wh) {
                if ($(window).height() < 600) {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                } else {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                }
            },
            afterRemove: function() {
                // 
            },
        })
        //  Return the module for AMD compliance.
        return Page;
    })
