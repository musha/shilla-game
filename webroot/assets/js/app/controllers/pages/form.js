// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(['vendor/zepto/zepto.html5Loader.min'],
                function() {
                    done();
                });
            },
            afterRender: function() {

                // 自适应
                $('.modal').height($(window).height());
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                // 调整手机屏幕尺寸 
                if ($(window).height() < 600) {
                    //
                } else {
                    // 
                };

                var context = this;

                //动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.form_page'), 0.5, { autoAlpha: 0, onComplete: function() {
                    // console.log('page form');
                } }, 0.1);

                // 提交信息
                context.$('.btn-form').on('click', function() {
                    $('.btn-form').attr('disabled', true);
                    var username = $('input[name=username]').val();
                    var enname = $('input[name=enname]').val();
                    var passport = $('input[name=passport]').val();
                    var mobile = $('input[name=mobile]').val();
                    if (username.length < 1) {
                        alert('请输入姓名');
                        $('.btn-form').attr('disabled', false);
                        return;
                    };
                    if (enname.length < 1) {
                        alert('请输入拼音');
                        $('.btn-form').attr('disabled', false);
                        return;
                    };
                    if (passport.length < 1) {
                        alert('请输入护照号');
                        $('.btn-form').attr('disabled', false);
                        return;
                    };
                    var telReg = /^[1][0-9]{10}$/;
                    if (mobile == '') {
                        alert('请输入手机号码');
                        $('.btn-form').attr('disabled', false);
                        return;
                    } else if (!telReg.test(mobile)) {
                        alert('请输入有效的手机号码');
                        $('.btn-form').attr('disabled', false);
                        return;
                    };
                    $.ajax({
                        type: 'post',
                        url: app.httpUrl + '/interface/api.aspx?type=saveinfo&r=' + Math.random(),
                        dataType: 'json',
                        data: {
                            openid: $.cookie('openid'),
                            name: username,
                            enname: enname,
                            passport: passport,
                            phone: mobile,
                            gidx: app.gidx,//抽奖接口gift返回的值
                            gguid: app.gguid//抽奖接口gift返回的值
                        },
                        async: false,
                        cache: false,
                        success: function (result) {
                            if (result.errcode == 1) {
                                // alert('提交成功');
                                tl.kill();
                                app.router.goto('message-success', ['form']);
                            } else if (result.errcode == -1) {
                                alert('未授权');
                                $('.btn-form').attr('disabled', false);
                            } else {
                                alert('失败：' + result.errmsg);
                                $('.btn-form').attr('disabled', false);
                            };
                        }
                    });
                });
            },
            resize: function(ww, wh) {
                if ($(window).height() < 600) {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                } else {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                }
            },
            afterRemove: function() {
                // 
            },
        })
        //  Return the module for AMD compliance.
        return Page;
    })
