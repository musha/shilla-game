// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(['vendor/zepto/zepto.html5Loader.min', 'vendor/ui/awardRotate'],
                function() {
                    done();
                });
            },
            afterRender: function() {

                // 自适应
                $('.modal').height($(window).height());
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                // 调整手机屏幕尺寸 
                if ($(window).height() > 700) {
                    $('.title').css('top', '6%');
                    $('.turntable-wrapper').css('top', '32%');
                    $('.bottom').css('bottom', '10%');
                } else {
                    // 
                };

                // 默认没同意
                app.isAgree = false;
                // 用户同意过后就不再弹出说明
                console.log('用户是否同意：' + $.cookie('isAgree'));
                if($.cookie('isAgree')) {
                    $('.modal-rules').hide();
                };

                // 剩余抽奖次数
                $('.time-num').text(app.leftTime);

                // 保持五等奖弹窗
                if(app.isFromTurn) {
                    $('.modal-prize5').fadeIn('fast');
                };

                var context = this;

                //动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.turntable_page'), 0.5, { autoAlpha: 0, onComplete: function() {
                    // console.log('page turntable');
                } }, 0.1);

                // 同意
                $('.btn-agree').on('click', function() {
                    // app.isAgree = true;
                    $.cookie('isAgree', true, 365);
                    $('.modal-rules').fadeOut('fast');
                });

                // 拒绝
                $('.btn-refuse').on('click', function() {
                    tl.kill();
                    app.router.goto('home');
                });

                // 关闭
                $('.close').on('click', function() {
                    $('.modal').fadeOut('fast');
                });

                // 转盘config
                var turnplate = {
                    restaraunts: ['金猪 (88g)', '三星笔记本', '新罗首尔酒店住宿券(1晚)\n携程优惠券(50万韩元)', '新罗购物卡\n(8万韩元)', '新罗会员\n专属优惠'],             //大转盘奖品名称
                    colors:['#fff', '#ff9f1a', '#fecc2f', '#ff9f1a', '#fecc2f'],                  //大转盘奖品区块对应背景颜色
                    outsideRadius: 180,          //大转盘外圆的半径
                    textRadius: 160,             //大转盘奖品位置距离圆心的距离
                    insideRadius: 0,            //大转盘内圆的半径
                    startAngle: 0,               //开始角度
                    bRotate: false               //false:停止;ture:旋转
                };

                // 转盘函数
                function drawRouletteWheel() {
                    // console.log('开始渲染');    
                    var canvas = document.getElementById('wheelcanvas');   
                    if (canvas.getContext) {
                        //根据奖品个数计算圆周角度
                        var arc = Math.PI / (turnplate.restaraunts.length/2);
                        var ctx = canvas.getContext('2d');
                        //在给定矩形内清空一个矩形
                        ctx.clearRect(0, 0, 422, 422);
                        //strokeStyle 属性设置或返回用于笔触的颜色、渐变或模式  
                        ctx.strokeStyle = '#FFBE04';
                        //font 属性设置或返回画布上文本内容的当前字体属性
                        ctx.font = '13px Microsoft YaHei';      
                        for(var i = 0; i < turnplate.restaraunts.length; i++) {       
                            var angle = turnplate.startAngle + i * arc;
                            ctx.fillStyle = turnplate.colors[i];
                            ctx.beginPath();
                            //arc(x,y,r,起始角,结束角,绘制方向) 方法创建弧/曲线（用于创建圆或部分圆）    
                            ctx.arc(211, 211, turnplate.outsideRadius, angle, angle + arc, false);    
                            ctx.arc(211, 211, turnplate.insideRadius, angle + arc, angle, true);
                            ctx.stroke();  
                            ctx.fill();
                            //锁画布(为了保存之前的画布状态)
                            ctx.save();   
                          
                            //----绘制奖品开始----
                            ctx.fillStyle = '#51221c'; // 文字的颜色
                            var text = turnplate.restaraunts[i];
                            var line_height = 19;
                            //translate方法重新映射画布上的 (0,0) 位置
                            ctx.translate(211 + Math.cos(angle + arc / 2) * turnplate.textRadius, 211 + Math.sin(angle + arc / 2) * turnplate.textRadius);
                          
                            //rotate方法旋转当前的绘图
                            ctx.rotate(angle + arc / 2 + Math.PI / 2);
                          
                            /** 下面代码根据奖品类型、奖品名称长度渲染不同效果，如字体、颜色、图片效果。(具体根据实际情况改变) **/
                            //if(text.indexOf('首尔') > 0) {
                                //新罗首尔酒店住宿券
                            var texts = text.split('\n');
                            for(var j = 0; j<texts.length; j++) {
                                // ctx.font = j == 0 ? '14px Microsoft YaHei' : '14px Microsoft YaHei';
                                if(j == 0){
                                    ctx.fillText(texts[j], -ctx.measureText(texts[j]).width / 2, j * line_height);
                                } else {
                                    ctx.fillText(texts[j], -ctx.measureText(texts[j]).width / 2, j * line_height);
                                };
                            };
                            // } else if(text.indexOf('M') == -1 && text.length > 6) {//奖品名称长度超过一定范围 
                                // text = text.substring(0,6) + '||' + text.substring(6);
                                // var texts = text.split('||');
                                // for(var j = 0; j < texts.length; j++)  {
                                //     ctx.fillText(texts[j], -ctx.measureText(texts[j]).width / 2, j * line_height);
                                // };
                            // } else {
                                //在画布上绘制填色的文本。文本的默认颜色是黑色
                                //measureText()方法返回包含一个对象，该对象包含以像素计的指定字体宽度
                                // ctx.fillText(text, -ctx.measureText(text).width / 2, 0);
                            // }
                          
                            //添加对应图标
                            if(text.indexOf('优惠') > 0) {
                                var img = document.getElementById('coupon');
                                img.onload = function() {
                                    ctx.drawImage(img, -44, 24, 84, 60);      
                                }; 
                                ctx.drawImage(img, -44, 24, 84, 60); 
                            };
                            if(text.indexOf('购物') > 0) {
                                var img = document.getElementById('coupon2');
                                img.onload = function() {  
                                    ctx.drawImage(img, -44, 24, 84, 60);      
                                };  
                                ctx.drawImage(img, -44, 24, 84, 60);  
                            };
                            if(text.indexOf('三星') >= 0) {
                                var img = document.getElementById('samsung');
                                img.onload = function() {  
                                    ctx.drawImage(img, -38, 20, 80, 56.25);      
                                };  
                                ctx.drawImage(img, -38, 20, 80, 56.25);
                            };
                            if(text.indexOf('金') >= 0) {
                                var img = document.getElementById('gold-pig');
                                img.onload = function() {  
                                    ctx.drawImage(img, -31, 18, 54, 57.3);      
                                };  
                                ctx.drawImage(img, -31, 18, 54, 57.3);
                            };
                            //把当前画布返回（调整）到上一个save()状态之前 
                            ctx.restore();
                            //----绘制奖品结束----
                        }     
                    };
                    // console.log('结束渲染'); 
                };

                //渲染转盘
                setTimeout(function(){
                    drawRouletteWheel();
                }, 500);

                // 产生随机数
                function rnd(n, m){
                    var random = Math.floor(Math.random()*(m-n+1)+n);
                    return random;
                };
                
                // 抽奖函数
                // app.gidx = 0, app.gguid = '';
                function gift(){
                    $.ajax({
                        type: 'post',
                        url: app.httpUrl + '/interface/api.aspx?type=gift&r=' + Math.random(),
                        dataType: 'json',
                        data: { openid: $.cookie('openid') },
                        async: false,
                        cache: false,
                        success: function (result) {
                            console.log(result);
                            app.leftTime = result.leftnum - 1;
                            if (result.errcode == 1) {
                                //抽奖成功
                                app.giftid = result.giftid;
                                app.gidx = result.gidx;//调用提交信息接口saveinfo时作为参数传入
                                app.gguid = result.gguid;//调用提交信息接口saveinfo时作为参数传入
                                //奖品数量等于10,指针落在对应奖品区域的中心角度[252, 216, 180, 144, 108, 72, 36, 360, 324, 288]
                                // 剩余抽奖次数
                                $('.time-num').text(app.leftTime); 
                                rotateFn(result.giftid);
                            } else if (result.errcode == -1) {
                                alert('未授权');
                                // $('.turnplate-button').attr('disabled', false);
                            } else {
                                alert('失败：' + result.errmsg);
                                $('.turnplate-button').attr('disabled', false);
                            }
                        }
                    });
                };

                //旋转转盘 item:奖品位置; txt：提示语;
                function rotateFn(item){
                    var angles = item * (360 / turnplate.restaraunts.length) - (360 / (turnplate.restaraunts.length*2));
                    if(angles < 270) {
                        angles = 270 - angles; 
                    } else{
                        angles = 360 - angles + 270;
                    };
                    $('.pan').stopRotate();
                    $('.pan').rotate({
                        angle: 0,
                        animateTo: angles + 1800,
                        duration: 8000,
                        callback: function(){
                            if(item < 5) {
                                // 1-4等奖
                                $('.modal-prize' + item).fadeIn('fast');
                            } else {
                                // 5等奖
                                $('.modal-prize5').fadeIn('fast');
                            };  
                            turnplate.bRotate = !turnplate.bRotate;
                            $('.turnplate-button').attr('disabled', false);
                        }
                    });
                };

                // 门店代金券
                $('.shop-coupon').on('click', function() {
                    gtag('event', 'coupon1', {'event_category': 'airport', 'event_label': 'click'});
                    tl.kill();
                    app.isFromTurn = true;
                    app.router.goto('coupons', ['turntable']);
                });

                // 购物卡
                $('.shopping-card').on('click', function() {
                    gtag('event', 'coupon2', {'event_category': 'airport', 'event_label': 'click'});
                    tl.kill();
                    app.isFromTurn = true;
                    app.router.goto('card', ['turntable']);
                });

                // 日期
                function FormatDate(strTime) {
                    var date = new Date(strTime);
                    return date.getFullYear() + "-" + (date.getMonth() + 1 ) + "-" + date.getDate();
                };

                // 报销出租车截止日期
                $('.taxi').on('click', function(e) {
                    gtag('event', 'coupon3', {'event_category': 'airport', 'event_label': 'click'});
                    var today = new Date();
                    var today_time =  FormatDate(today);
                    if(today_time > '2019-01-01'){
                        e.preventDefault();
                        tl.kill();
                        app.isFromTurn = true;
                        app.router.goto('message-fin', ['turntable']);
                    }
                });

                // 开通银卡
                $('.silver-card').on('click', function() {
                    gtag('event', 'coupon4', {'event_category': 'airport', 'event_label': 'click'});
                });

                // 再抽一次
                $('.btn-again').on('click', function() {
                    gtag('event', 'retry', {'event_category': 'airport', 'event_label': 'click'});
                    tl.kill();
                    if(app.leftTime > 0) {
                        app.isFromTurn = false;
                        $('.modal').fadeOut('fast');
                    } else {
                        app.isFromTurn = true;
                        app.router.goto('message-over');
                    }
                });
                 
                // 超时函数
                function rotateTimeOut(){
                    $('.pan').rotate({
                        angle: 0,
                        animateTo: 2160,
                        duration: 8000,
                        callback:function (){
                            alert('网络超时，请检查您的网络设置！');
                        }
                    });
                };

                // 抽奖
                $('.turnplate-button').on('click', function (){
                    gtag('event', 'start', {'event_category': 'airport', 'event_label': 'click'});
                    if(app.leftTime == 0) {
                        tl.kill();
                        app.router.goto('message-over');
                        return;
                    };
                    $('.turnplate-button').attr('disabled', true);
                    //调用抽奖函数
                    gift();
                    if(turnplate.bRotate) {
                        return;
                    };
                    turnplate.bRotate = !turnplate.bRotate;
                });

                // 领奖
                $('.btn-apply').on('click', function() {
                    tl.kill();
                    app.router.goto('policy', ['turntable']);
                });

                // 分享
                $('.btn-share').on('click', function() {
                    gtag('event', 'share2', {'event_category': 'airport','event_label': 'click'});
                    tl.kill();
                    if(app.leftTime > 0) {
                        app.isFromTurn = true;
                    };
                    app.router.goto('message-share', ['turntable']);
                });             
            },
            resize: function(ww, wh) {
                if ($(window).height() < 600) {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                } else {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                }
            },
            afterRemove: function() {
                // 
            },
        })
        //  Return the module for AMD compliance.
        return Page;
    })
