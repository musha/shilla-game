// Page module
define(["app", "controllers/base/page"],

    function(app, BasePage) {
        var Page = {};

        Page.View = BasePage.View.extend({
            fitOn: "width", //width, height, custom
            beforeRender: function() {
                var done = this.async();
                require(['vendor/zepto/zepto.html5Loader.min'],
                function() {
                    done();
                });
            },
            afterRender: function() {

                // 自适应
                $('.modal').height($(window).height());
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                // 调整手机屏幕尺寸 
                if ($(window).height() < 600) {
                    //
                } else {
                    // 
                };

                var context = this;

                //动画效果
                var tl = new TimelineMax();
                tl.from(context.$('.prize-coupons_page'), 0.5, { autoAlpha: 0, onComplete: function() {
                    // console.log('page prize-coupons');
                } }, 0.1);

                // 门店代金券
                $('.shop-coupon').on('click', function() {
                    gtag('event', 'coupon1', {'event_category': 'airport', 'event_label': 'click'});
                    tl.kill();
                    app.router.goto('coupons', ['prize-coupons']);
                });

                // 购物卡
                $('.shopping-card').on('click', function() {
                    gtag('event', 'coupon2', {'event_category': 'airport', 'event_label': 'click'});
                    tl.kill();
                    app.router.goto('card', ['prize-coupons']);
                });

                // 日期
                function FormatDate(strTime) {
                    var date = new Date(strTime);
                    return date.getFullYear() + "-" + (date.getMonth() + 1 ) + "-" + date.getDate();
                };

                // 报销出租车截止日期
                $('.taxi').on('click', function(e) {
                    gtag('event', 'coupon3', {'event_category': 'airport', 'event_label': 'click'});
                    var today = new Date();
                    var today_time =  FormatDate(today);
                    if(today_time > '2019-01-01'){
                        e.preventDefault();
                        tl.kill();
                        app.router.goto('message-fin', ['prize-coupons']);
                    }
                });

                // 开通银卡
                $('.silver-card').on('click', function() {
                    gtag('event', 'coupon4', {'event_category': 'airport', 'event_label': 'click'});
                });

                // 分享
                $('.btn-share').on('click', function() {
                    gtag('event', 'share2', {'event_category': 'airport','event_label': 'click'});
                    tl.kill();
                    app.router.goto('message-share', ['prize-coupons']);
                });
            },
            resize: function(ww, wh) {
                if ($(window).height() < 600) {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                } else {
                    if (app.s >= 100) {
                        //
                    } else {
                        //
                    }
                }
            },
            afterRemove: function() {
                // 
            },
        })
        //  Return the module for AMD compliance.
        return Page;
    })
