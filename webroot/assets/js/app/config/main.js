require(["app", "managers/layout-manager", "managers/router", "managers/resize-manager", "managers/tracker", "managers/ui-framework7"],


    function(app, LayoutManager, Router, ResizeManager, Tracker, Framework7) {

        // layout manager
        app.layout = (new LayoutManager()).layout();

        // router
        app.router = new Router();

        app.resizeManager = new ResizeManager();

        app.framework7 = new Framework7();

        app.tracker = new Tracker({ gaAccount: "", baiduAccount: "" });

        app.eventBus = _.extend({}, Backbone.Events);

        // 接口地址
        // app.baseUrl = 'http://campaign.lancome.com.cn/genshenzhen2018';

        /*
         app.user = new User.Model();
         app.user.getInfo();
         */

        app.layout.render().promise().done(function() {
            Backbone.history.start({
                pushState: false
            });
        });

        // 微信不分享
        // function onBridgeReady() { 
        //     WeixinJSBridge.call('hideOptionMenu'); 
        // };
        // if (typeof WeixinJSBridge == "undefined") { 
        //     if (document.addEventListener) { 
        //         document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false); 
        //     } else if (document.attachEvent) { 
        //         document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
        //         document.attachEvent('onWeixinJSBridgeReady', onBridgeReady); 
        //     } 
        // } else { 
        //     onBridgeReady(); 
        // };

        //微信分享
        // var shareUrl = "http://campaign.archisense.cn/ShillaTravelQuiz";


        // app.shareDataFriend = {
        //     share: {
        //         title: '挑对合拍旅行地，放飞自己很容易！',
        //         desc: ' 三分钟答题，解锁你的旅行合拍地！',
        //         link: shareUrl,
        //         imgUrl: "http://shilladfs.shjiaoai.com/webroot/assets/images/share.jpg",
        //         success: function() {
        //             // gtag('event', 'share_friend', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
        //             // app.tracker.event('shared', 'event', 'share');

        //         }
        //     }
        // };
        // app.shareDataTimeLine = {
        //     share: {
        //         title: '挑对合拍旅行地，放飞自己很容易！',
        //         desc: ' 三分钟答题，解锁你的旅行合拍地！',
        //         link: shareUrl,
        //         imgUrl: "http://shilladfs.shjiaoai.com/webroot/assets/images/share.jpg",
        //         success: function() {
        //             // gtag('event', 'share_moment', {'event_category': 'shilla2018Q3', 'event_label': 'click' });
        //             // app.tracker.event('shared', 'event', 'share');

        //         }
        //     }
        // };

        // if (window.location.host.indexOf("shilladfs.shjiaoai.com") != -1) {
        //     require(["//res.wx.qq.com/open/js/jweixin-1.0.0.js"],
        //     function(wx) {
        //         app.wx = wx;
        //         $.ajax({
        //             url: '/wechat/api/share.php',
        //             data: { url: document.location.href.split("#")[0] },
        //             cache: false,
        //             success: function(data) {
        //                 app.wx.config({
        //                     "debug": false,
        //                     "appId": data.appId,
        //                     "timestamp": data.timestamp,
        //                     "nonceStr": data.nonceStr,
        //                     "signature": data.signature,
        //                     "jsApiList": ["onMenuShareTimeline", "onMenuShareAppMessage", "onMenuShareQQ", "onMenuShareWeibo", "onMenuShareQZone"]
        //                 });
        //                 app.wx.ready(function() {
        //                     var shareNum = Math.floor(Math.random() * 3);
        //                     app.wx.onMenuShareAppMessage(app.shareDataFriend.share);
        //                     app.wx.onMenuShareTimeline(app.shareDataTimeLine.share);
        //                 });
        //             },
        //             error: function(XHR, textStatus, errorThrown) {
        //             }
        //         });
        //     });
        // }

        // 判断是正式还是测试, 默认是正式
        app.httpUrl = 'http://shilladfs.shjiaoai.com';
        if(window.location.host.indexOf('test') >= 0) {
            // 测试
            app.httpUrl = 'http://shilladfs-test.shjiaoai.com';
        };

        var shareData = {
            arr: [
                {
                    img_url: app.httpUrl + '/webroot/assets/images/share-img.jpg',
                    site_link: app.httpUrl + '/webroot/',
                    messageDesc: '瓜分新罗豪礼，就现在！',
                    circleDesc: '瓜分新罗豪礼，就现在！',
                    title: '喜提2019开运豪礼，够我吹一年！',
                    backFun: "",
                    cancelFun: ""
                },
                {
                    img_url: app.httpUrl + '/webroot/assets/images/share-img.jpg',
                    site_link: app.httpUrl + '/webroot/',
                    messageDesc: '瓜分新罗豪礼，就现在！',
                    circleDesc: '瓜分新罗豪礼，就现在！',
                    title: '转发这只新罗金猪，2019“猪”事顺利！',
                    backFun: "",
                    cancelFun: ""
                },
                {
                    img_url: app.httpUrl + '/webroot/assets/images/share-img.jpg',
                    site_link: app.httpUrl + '/webroot/',
                    messageDesc: '瓜分新罗豪礼，就现在！',
                    circleDesc: '瓜分新罗豪礼，就现在！',
                    title: '2019“猪”事顺利，新罗豪礼享不停！',
                    backFun: "",
                    cancelFun: ""
                }
            ] 
        };

        function setShareData(index) {
            wx.onMenuShareTimeline({
                title: shareData.arr[index].title,
                desc: shareData.arr[index].circleDesc,
                link: shareData.arr[index].site_link,
                imgUrl: shareData.arr[index].img_url,
                trigger: function (res) {
                  
                },
                success: function (res) {
                  
                },
                cancel: function (res) {
                },
                fail: function (res) {
                }
            });
            wx.onMenuShareAppMessage({
                title: shareData.arr[index].title,
                desc: shareData.arr[index].circleDesc,
                link: shareData.arr[index].site_link,
                imgUrl: shareData.arr[index].img_url,
                type: "",
                dataUrl: "",
                success: function () {
                },
                trigger: function (res) {
                },
                cancel: function () {
                    
                }
            });

            wx.onMenuShareQZone({
                title: shareData.arr[index].title, // 分享标题
                desc: shareData.arr[index].messageDesc, // 分享描述
                link: shareData.arr[index].site_link, // 分享链接
                imgUrl: shareData.arr[index].img_url, // 分享图标
                success: function () { 
                   // 用户确认分享后执行的回调函数
                },
                cancel: function () { 
                    // 用户取消分享后执行的回调函数
                }
            });


            wx.onMenuShareQQ({
                title: shareData.arr[index].title, // 分享标题
                desc: shareData.arr[index].messageDesc, // 分享描述
                link: shareData.arr[index].site_link, // 分享链接
                imgUrl: shareData.arr[index].img_url, // 分享图标
                success: function () {
                  // 用户确认分享后执行的回调函数
                },
                cancel: function () {
                  // 用户取消分享后执行的回调函数
                  
                }
            });
        }
        wx.ready(function()
        {   
            var shareNum = Math.floor(Math.random() * 3);
            setShareData(shareNum);
        });

        function closewin() {
            wx.closeWindow();
        } 

        $.ajax({
            type: 'post',
            url: 'http://babyau.shjiaoai.com/data/api.aspx?r=' + Math.random(),
            dataType: 'text',
            data: {
                type: 'signature',
                url: window.location.href.split("#")[0],
                weixinidx: 1
            },
            async: false,
            cache: false,
            success: function (result) {
                result = $.parseJSON(result)
                wx.config(
                {
                    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，
                    appId: result.appid,
                    timestamp: result.timestamp, // 必填，生成签名的时间戳
                    nonceStr: result.nonceStr, // 必填，生成签名的随机串
                    signature: result.signature,// 必填，签名，见附录1
                    jsApiList: [
                      'onMenuShareTimeline',
                      'onMenuShareAppMessage',
                      'onMenuShareQQ',
                      'onMenuShareWeibo',
                      'onMenuShareQZone',
                      'hideMenuItems',
                      'showMenuItems',
                      'hideAllNonBaseMenuItem',
                      'showAllNonBaseMenuItem',
                      'translateVoice',
                      'startRecord',
                      'stopRecord',
                      'onVoiceRecordEnd',
                      'playVoice',
                      'onVoicePlayEnd',
                      'pauseVoice',
                      'stopVoice',
                      'uploadVoice',
                      'downloadVoice',
                      'chooseImage',
                      'previewImage',
                      'uploadImage',
                      'downloadImage',
                      'getNetworkType',
                      'openLocation',
                      'getLocation',
                      'hideOptionMenu',
                      'showOptionMenu',
                      'closeWindow',
                      'scanQRCode',
                      'chooseWXPay',
                      'openProductSpecificView',
                      'addCard',
                      'chooseCard',
                      'openCard'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
                });
            },
            fail: function (e) { }
        });

    });