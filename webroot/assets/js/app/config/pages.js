define([
        "controllers/pages/loading",
        "controllers/pages/home",
        "controllers/pages/turntable",
        "controllers/pages/policy",
        "controllers/pages/form",
        "controllers/pages/prize-coupons",
        "controllers/pages/coupons",
        "controllers/pages/card",
        "controllers/pages/message-success",
        "controllers/pages/message-share",
        "controllers/pages/message-fin",
        "controllers/pages/message-over",
    ],
    function(Loading, Home, Turntable, Policy, Form, PrizeCoupons, Coupons, Card, MessageSuccess, MessageShare, MessageFin, MessageOver) {
        var pages = [
            {
                routeId: 'loading',
                type: 'main',
                landing: false,
                page: function() {
                    return new Loading.View({ template: "pages/loading" });
                }
            },
            {
                routeId: 'home',
                type: 'main',
                landing: true,
                page: function() {
                    return new Home.View({ template: "pages/home" });
                }
            },
            {
                routeId: 'turntable',
                type: 'main',
                landing: false,
                page: function() {
                    return new Turntable.View({ template: "pages/turntable" });
                }
            },
            {
                routeId: 'policy',
                type: 'main',
                landing: false,
                page: function() {
                    return new Policy.View({ template: "pages/policy" });
                }
            },
            {
                routeId: 'form',
                type: 'main',
                landing: false,
                page: function() {
                    return new Form.View({ template: "pages/form" });
                }
            },
            {
                routeId: 'prize-coupons',
                type: 'main',
                landing: false,
                page: function() {
                    return new PrizeCoupons.View({ template: "pages/prize-coupons" });
                }
            },
            {
                routeId: 'coupons',
                type: 'main',
                landing: false,
                page: function() {
                    return new Coupons.View({ template: "pages/coupons" });
                }
            },
            {
                routeId: 'card',
                type: 'main',
                landing: false,
                page: function() {
                    return new Card.View({ template: "pages/card" });
                }
            },
            {
                routeId: 'message-success',
                type: 'main',
                landing: false,
                page: function() {
                    return new MessageSuccess.View({ template: "pages/message-success" });
                }
            },
            {
                routeId: 'message-share',
                type: 'main',
                landing: false,
                page: function() {
                    return new MessageShare.View({ template: "pages/message-share" });
                }
            },
            {
                routeId: 'message-fin',
                type: 'main',
                landing: false,
                page: function() {
                    return new MessageFin.View({ template: "pages/message-fin" });
                }
            },
            {
                routeId: 'message-over',
                type: 'main',
                landing: false,
                page: function() {
                    return new MessageOver.View({ template: "pages/message-over" });
                }
            },
        ];
        return pages;
    });